/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_struct.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/15 21:31:30 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:34 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

t_printf	*ft_struct(void)
{
	t_printf	*lst;
	int			i;

	i = 0;
	if (!(lst = (t_printf *)malloc(sizeof(*lst))))
		return (NULL);
	lst->str = NULL;
	lst->wstr = NULL;
	lst->print = NULL;
	lst->wprint = NULL;
	while (i < 5)
		lst->flag[i++] = 0;
	lst->width = 0;
	lst->prec = 0;
	lst->type = 0;
	lst->length = 0;
	lst->slen = 0;
	lst->plen = 0;
	lst->neg = 0;
	lst->error = 0;
	return (lst);
}
