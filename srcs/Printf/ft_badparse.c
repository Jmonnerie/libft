/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_badparse.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/15 23:13:28 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:30 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_badparse(char *form, long int nbr, t_printf *lst, char **pstr)
{
	char	*str;
	int		ret;

	str = *pstr;
	if (lst->error == -1)
	{
		*pstr = str + 1;
		ft_free_pf(lst);
		return (-1);
	}
	while (*str != 0 && ft_strchr("#+ -0123456789*.hljz%", *str))
		str++;
	if (*str)
	{
		lst->type = 'c';
		lst->str = ft_chartostr(*str);
		ft_cmpchars(lst);
		*pstr = str;
		ret = write(1, form, nbr);
		ret += write(1, lst->print, lst->plen);
		ft_free_pf(lst);
		return (ret);
	}
	*pstr = str - 1;
	return (write(1, form, nbr));
}
